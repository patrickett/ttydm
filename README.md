
- [Installation](https://gitlab.com/patrickett/ttydm#installation)
- [Usage](https://gitlab.com/patrickett/ttydm#usage)
- [Bugs](https://gitlab.com/patrickett/ttydm#bugs)
- [Contributing](https://gitlab.com/patrickett/ttydm#contributing)
- [License](https://gitlab.com/patrickett/ttydm#license)

**ttydm** is a low dependency display manager.
Depends on `ncurses` and `linux-pam`.

It is made so that [X11](https://en.wikipedia.org/wiki/X.Org_Server) and [systemd](https://en.wikipedia.org/wiki/Systemd) are optional.

# Installation


Nothing concrete yet.  

Create executable

    gcc ttydm.c -lmenu -lform -lncurses -lpam -lpam_misc -o ttydm

Move to /usr/bin

    sudo cp ttydm /usr/bin/ttydm

Move service file to systemd service location    

    sudo cp ttydm.service /lib/systemd/system/ttydm.service

Enable systemd service

    sudo systemctl enable ttydm.service

Disable getty screen for tty1 so you can see ttydm

    sudo systemctl disable getty@tty1.service


# Bugs

You can submit issues here on the [Gitlab issues page](https://gitlab.com/patrickett/ttydm/issues).

# Contributing

If you would like to contribute or suggest any improvements to the project, please submit a merge request or open a issue with a feature request.
