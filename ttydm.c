// show caps lock light
// create a save it /etc/ with the username
// make runit service
// child pid for de/wm
// de/wm switcher
// /usr/share/wayland-sessions/* /usr/share/xsessions/*

#include <ncurses.h>
#include <form.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h> // for getting hostname
#include <stdio.h>
#include <security/pam_appl.h>

struct pam_response *reply;

char *user_field, *pass_field;

static FORM *form;
static FIELD *fields[5];
static WINDOW *win_body, *win_form;

int set_xdg_runtime_dir() {
  char *xdgvar = getenv("XDG_RUNTIME_DIR");
  char xdgstring[100];
  if (xdgvar == NULL) {
    sprintf(xdgstring, "/tmp/%d-runtime-dir", getuid());
    setenv("XDG_RUNTIME_DIR", xdgstring, 0);
    // mvprintw(2, 10, "%s", xdgstring);
  }
  return 0;
}

// //function used to get user input
int function_conversation(int num_msg, const struct pam_message **msg, struct pam_response **resp, void *appdata_ptr)
{
    *resp = reply;
    return PAM_SUCCESS;
}

int authenticate_system(const char *username, const char *password)
{
    const struct pam_conv local_conversation = { function_conversation, NULL };
    pam_handle_t *local_auth_handle = NULL; // this gets set by pam_start

    int retval;
    retval = pam_start("su", username, &local_conversation, &local_auth_handle);

    if (retval != PAM_SUCCESS)
    {
            // printf("pam_start returned: %d\n ", retval);
            return 0;
    }

    reply = (struct pam_response *)malloc(sizeof(struct pam_response));

    // would like to replace strdup with strncpy() as strdup is not in c standard library
    // also strdup is defined implcitily here
    reply[0].resp = strdup(password);
    reply[0].resp_retcode = 0;
    retval = pam_authenticate(local_auth_handle, 0);

    if (retval != PAM_SUCCESS)
    {
            if (retval == PAM_AUTH_ERR)
            {
                    // printf("Authentication failure.\n");
                    // mvprintw(20 , 5, "Authentication failure");
            }
            else
            {
                // printf("pam_authenticate returned %d\n", retval);
            }
            return 0;
    }
    set_xdg_runtime_dir();
    mvwprintw(win_body, 15, 29, "Authentication Success");
    wrefresh(win_body);
    system("sway");
    // mvprintw(20 , 5, "Authenticated");
    // printf("Authenticated.\n");
    retval = pam_end(local_auth_handle, retval);
    if (retval != PAM_SUCCESS)
    {
            // printf("pam_end returned\n");
            return 0;
    }
    return 1;
}

int auth_user()
{
    char* login;
    char* password;
    login = user_field;
    password = pass_field;
    if (authenticate_system(login, password) == 1)
    {
        return 0;
    }
    // printf("Authentication failed!\n");
    mvwprintw(win_body, 15, 29, "Authentication Failure");
    wrefresh(win_body);
    return 1;
}
//This is useful because ncurses fill fields blanks with spaces.
static char* trim_whitespaces(char *str)
{
	char *end;
	// trim leading space
	while(isspace(*str))
		str++;
	if(*str == 0) // all spaces?
		return str;
	// trim trailing space
	end = str + strnlen(str, 128) - 1;
	while(end > str && isspace(*end))
		end--;
	// write new null terminator
	*(end+1) = '\0';
	return str;
}

static void driver(int ch)
{

	switch (ch) {
		case 10:
	  // Or the current field buffer won't be sync with what is displayed
	  form_driver(form, REQ_NEXT_FIELD);
	  form_driver(form, REQ_PREV_FIELD);
	  move(LINES-3, 2);
      user_field = trim_whitespaces(field_buffer(fields[1], 0));
      pass_field = trim_whitespaces(field_buffer(fields[3], 0));
      // printw("%s", user_field);
      // printw("%s", pass_field);
      auth_user();
	  refresh();
	  pos_form_cursor(form);
	  break;

    case KEY_F(1):
      execl("/bin/shutdown", "shutdown", "-P", "now", (char *)0);
      break;

    case KEY_F(5):
      execl("/bin/shutdown", "shutdown", "-r", "now", (char *)0);
      break;

    case 360: // end key
      form_driver(form, REQ_END_LINE);
      break;

    case 262: // home key
      form_driver(form, REQ_BEG_LINE);
      break;

		case KEY_DOWN:
			form_driver(form, REQ_NEXT_FIELD);
			form_driver(form, REQ_END_LINE);
			break;

		case KEY_UP:
			form_driver(form, REQ_PREV_FIELD);
			form_driver(form, REQ_END_LINE);
			break;

		case '\t': // grabs the tab key
			form_driver(form, REQ_NEXT_FIELD);
			form_driver(form, REQ_END_LINE);
			break;

		case KEY_LEFT:
			form_driver(form, REQ_PREV_CHAR);
			break;

		case KEY_RIGHT:
			form_driver(form, REQ_NEXT_CHAR);
			break;

		case KEY_BACKSPACE:
		case 127: // Delete the char before cursor
			form_driver(form, REQ_DEL_PREV);
			break;

		case KEY_DC: // Delete the char under the cursor
			form_driver(form, REQ_DEL_CHAR);
			break;

		default:
			form_driver(form, ch);
			break;
	}

	wrefresh(win_form);
}

int main()
{
	int ch;
	initscr();
	noecho();
	cbreak();
	keypad(stdscr, TRUE);

    int gethostname(char *name, size_t len);
	char hostname[1024];
	hostname[1023] = '\0';
	gethostname(hostname, 1023); // get hostname
    size_t hn_length = strlen(hostname); // get hostname length

	int startx, starty, width, height, title, hn_placement; // variables for moving elements
	height = 20;
	width = 80;
	starty = (LINES - height) / 2;	/* Calculating for a center placement */
	startx = (COLS - width) / 2;	/* of the window		*/
	title = (COLS-20) /2;
	hn_placement = (width - hn_length) / 2; // for centering the hostname (replace the 10 with hostname char #)
	// printf("%d", hn_length );

	win_body = newwin(height, width, starty, startx);
	assert(win_body != NULL);
	box(win_body, 0, 0);
	win_form = derwin(win_body, height-10, width-2, 3, 1);
	assert(win_form != NULL);
	// box(win_form, 0, 0);
    // mvwprintw(win_body, 20 , hn_placement, "login");
	// mvprintw(2, title, "uid is %d", getuid());
    mvwprintw(win_body, 1, hn_placement, hostname);
    mvprintw(1, title, "ttydm version 0.02");
	mvprintw(LINES - 1, 0, "F1 to shutdown    F5 to reboot    F9 to exit to tty");

	fields[0] = new_field(1, 10, 1, 12, 0, 0);
	fields[1] = new_field(1, 40, 1, 22, 0, 0);
	fields[2] = new_field(1, 10, 4, 12, 0, 0);
	fields[3] = new_field(1, 40, 4, 22, 0, 0);
	fields[4] = NULL;
	assert(fields[0] != NULL && fields[1] != NULL && fields[2] != NULL && fields[3] != NULL);

	set_field_buffer(fields[0], 0, "Username:");
	set_field_buffer(fields[2], 0, "Password:");
	set_field_buffer(fields[1], 0, NULL);
	set_field_buffer(fields[3], 0, NULL);

	set_field_opts(fields[0], O_VISIBLE | O_PUBLIC | O_AUTOSKIP);
	set_field_opts(fields[1], O_VISIBLE | O_PUBLIC | O_EDIT | O_ACTIVE);
	set_field_opts(fields[2], O_VISIBLE | O_PUBLIC | O_AUTOSKIP);
	set_field_opts(fields[3], O_VISIBLE | O_EDIT | O_ACTIVE);

	set_field_back(fields[1], A_STANDOUT);
	set_field_back(fields[3], A_STANDOUT);

	form = new_form(fields);
	assert(form != NULL);
	set_form_win(form, win_form);
	set_form_sub(form, derwin(win_form, 5, 76, 1, 1));
	post_form(form);

	refresh();
	wrefresh(win_body);
	wrefresh(win_form);

	while ((ch = getch()) != KEY_F(9))
		driver(ch);

	unpost_form(form);
	free_form(form);
	free_field(fields[0]);
	free_field(fields[1]);
	free_field(fields[2]);
	free_field(fields[3]);
	delwin(win_form);
	delwin(win_body);
	endwin();

	return 0;
}
